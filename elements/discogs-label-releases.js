import LsLoading from './ls-loading.js'

export default class DiscogsLabelReleases extends HTMLElement {
	// the html custom-component properties we're watching
	static get observedAttributes() {
		return ['label-id']
	}

	discogsApiUrl = labelId => `https://api.discogs.com/labels/${labelId}/releases`

	get state() {
		return this._state || null
	}

	set state(data) {
		this._state = data || {}
		this.render()
	}

	constructor() {
		super()

		let labelId = this.getAttribute('label-id')
		this.fetchData(labelId).then(data => {
			this.state = data
		})

	}

	async fetchData(labelId) {
		const labelUrl = this.discogsApiUrl(labelId)
		const info = await fetch(labelUrl).then(data => data.json())
		return info
	}

	attributeChangedCallback() {
		this.render()
	}

	render() {
		if (!this.state) return this.innerHTML = `<ls-loading></ls-loading>`

		const {releases} = this.state

		let template = ''

		if (releases && releases.length) {
			template = `
		${ releases.reverse().map((r) => (`<discogs-release release-id=${r.id}></discogs-release>`)).join('') }
		`
		} else {
			template = `<div>No releases.</div>`
		}

		this.innerHTML = template
	}
}

customElements.define('discogs-label-releases', DiscogsLabelReleases)
