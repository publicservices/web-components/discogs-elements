export default class LsLoading extends HTMLElement {
	constructor() {
		super()
		this.innerHTML = `<span>...</span>`
	}
}

customElements.define('ls-loading', LsLoading)
