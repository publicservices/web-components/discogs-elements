const init = function() {
	var player = document.querySelector('radio4000-player')

	/* let's catch when track change, so we can do stuff */
	player.addEventListener('trackChanged', (event) => {
		const eventData = event.detail[0]
		console.info('trackChanged event', event.detail[0])

		/* it is the first track */
		if (!eventData || eventData.previousTrack || eventData.previousTrack.id) {
			document.querySelector('body').classList.add('is-player-visible')
		}
	})
}

export default init
